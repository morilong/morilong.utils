﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class TypeExtension
    {
        /// <summary>
        /// 判断一个 TypeCode 是否为数值类型
        /// </summary>
        /// <param name="typeCode"></param>
        /// <returns></returns>
        public static bool IsNumericType(this TypeCode typeCode)
        {
            var code = (int)typeCode;
            return code >= 5 && code <= 15;
        }

        /// <summary>
        /// 判断一个 Type 是否为数值类型
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNumericType(this Type type)
        {
            return IsNumericType(Type.GetTypeCode(type));
        }

        /// <summary>
        /// 根据 Type 获取默认值
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetDefaultValue(this Type type)
        {
            return type != null && type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        /// <summary>
        /// Type.GetTypeCode(type)
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static TypeCode GetTypeCode(this Type type)
        {
            return Type.GetTypeCode(type);
        }

    }
}
