﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace System.Security
{
    /// <summary>
    /// AES加密解密类
    /// </summary>
    public class AESCryptoHelper : CryptoHelper
    {
        #region 内部方法
        private string MD5(byte[] data)
        {
            byte[] resBytes = new MD5CryptoServiceProvider().ComputeHash(data);
            StringBuilder sb = new StringBuilder();
            foreach (byte item in resBytes)
            {
                sb.Append(item.ToString("X2"));
            }
            return sb.ToString();
        }
        /// <summary>
        /// 获取指定长度的md5值
        /// </summary>
        /// <param name="data"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private void GetMD5(ref byte[] data, AESBlockSize size)
        {
            switch (size)
            {
                case AESBlockSize.Bit_128:
                    if (data.Length != 16)
                        data = new MD5CryptoServiceProvider().ComputeHash(data);
                    return;
                case AESBlockSize.Bit_192:
                    if (data.Length != 24)
                        data = Encoding.Default.GetBytes(this.MD5(data).Substring(4, 24));
                    return;
                case AESBlockSize.Bit_256:
                    if (data.Length != 32)
                        data = Encoding.Default.GetBytes(this.MD5(data));
                    return;
            }
        }
        /// <summary>
        /// 根据key或iv值生成由AESBlockSize指定长度key和iv
        /// </summary>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private void GetKeyIV(ref byte[] key, ref byte[] iv, AESBlockSize size)
        {
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");

            this.GetMD5(ref key, size);

            if (iv == null || iv.Length <= 0)
            {
                iv = key;
            }
            else
            {
                this.GetMD5(ref iv, size);
            }
        }
        #endregion

        #region 公开属性

        private AESBlockSize _blockSize = AESBlockSize.Bit_256;
        /// <summary>
        /// 获取或设置加密操作的块大小. (默认: AESBlockSize.Bit_256)
        /// </summary>
        public AESBlockSize BlockSize
        {
            get { return this._blockSize; }
            set { this._blockSize = value; }
        }
        /// <summary>
        /// 获取或设置对称算法的初始化向量.
        /// </summary>
        public byte[] IV { get; set; }

        #endregion

        #region 构造方法
        /// <summary>
        /// 无参数初始化类的新实例.
        /// </summary>
        public AESCryptoHelper() { }
        /// <summary>
        /// 将 blockSize, key, iv 初始化类的新实例.
        /// </summary>
        /// <param name="key">密码</param>
        /// <param name="iv">初始化向量</param>
        /// <param name="blockSize">块大小</param> 
        public AESCryptoHelper(string key, string iv, AESBlockSize blockSize)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");

            base.Key = Encoding.Default.GetBytes(key);

            if (!string.IsNullOrEmpty(iv))
                this.IV = Encoding.Default.GetBytes(iv);

            this.BlockSize = blockSize;
        }
        /// <summary>
        /// 将 BlockSize为256, key, iv 初始化类的新实例.
        /// </summary>
        /// <param name="key">密码</param>
        /// <param name="iv">初始化向量</param>        
        public AESCryptoHelper(string key, string iv = null)
            : this(key, iv, AESBlockSize.Bit_256)
        {
        }
        /// <summary>
        /// 将 blockSize, key, iv 初始化类的新实例.
        /// </summary>
        /// <param name="key">密码</param>
        /// <param name="iv">初始化向量</param>
        /// <param name="blockSize">块大小</param> 
        public AESCryptoHelper(byte[] key, byte[] iv, AESBlockSize blockSize)
        {
            base.Key = key;
            this.IV = iv;
            this.BlockSize = blockSize;
        }
        /// <summary>
        /// 将 BlockSize为256, key, iv 初始化类的新实例.
        /// </summary>
        /// <param name="key">密码</param>
        /// <param name="iv">初始化向量</param>        
        public AESCryptoHelper(byte[] key, byte[] iv = null)
            : this(key, iv, AESBlockSize.Bit_256)
        {
        }
        #endregion

        #region 加密, 返回字节数组 + byte[] EncryptEx(byte[] data)
        /// <summary>
        /// 加密, 返回字节数组
        /// </summary>
        /// <param name="data">待加密数据</param>
        /// <returns></returns>
        public override byte[] EncryptEx(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            byte[] rgbKey = base.Key;
            byte[] rgbIV = this.IV;
            this.GetKeyIV(ref rgbKey, ref rgbIV, this.BlockSize);

            ICryptoTransform ct = new RijndaelManaged()
            {
                BlockSize = (int)this.BlockSize
            }.CreateEncryptor(rgbKey, rgbIV);

            return ct.TransformFinalBlock(data, 0, data.Length);
        }
        #endregion

        #region 解密由 EncryptEx 方法返回的数据, 返回字节数组 + byte[] DecryptEx(byte[] data)
        /// <summary>
        /// 解密由 EncryptEx 方法返回的数据, 返回字节数组
        /// </summary>
        /// <param name="data">待解密的数据, 由 EncryptEx 方法返回.</param>
        /// <returns></returns>
        public override byte[] DecryptEx(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            byte[] rgbKey = base.Key;
            byte[] rgbIV = this.IV;
            this.GetKeyIV(ref rgbKey, ref rgbIV, this.BlockSize);

            ICryptoTransform ct = new RijndaelManaged()
            {
                BlockSize = (int)this.BlockSize
            }.CreateDecryptor(rgbKey, rgbIV);

            return ct.TransformFinalBlock(data, 0, data.Length);
        }
        #endregion
    }

    /// <summary>
    /// 加密操作块大小
    /// </summary>
    public enum AESBlockSize
    {
        /// <summary>
        /// 128比特位，秘钥长度为16字节
        /// </summary>
        Bit_128 = 128,
        /// <summary>
        /// 192比特位，秘钥长度为24字节
        /// </summary>
        Bit_192 = 192,
        /// <summary>
        /// 256比特位，秘钥长度为32字节
        /// </summary>
        Bit_256 = 256,
    }
}
