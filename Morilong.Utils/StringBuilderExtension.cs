﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class StringBuilderExtension
    {
        public static StringBuilder AppendIf(this StringBuilder sb, bool condition, string value)
        {
            if (condition)
            {
                sb.Append(value);
            }
            return sb;
        }
        /// <summary>
        /// if(sb.Length > 0) sb.Append(value)
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder AppendIf(this StringBuilder sb, string value)
        {
            if (sb.Length > 0)
            {
                sb.Append(value);
            }
            return sb;
        }

        public static StringBuilder AppendLineIf(this StringBuilder sb, bool condition)
        {
            if (condition)
            {
                sb.AppendLine();
            }
            return sb;
        }
        /// <summary>
        /// if(sb.Length > 0) sb.AppendLine()
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder AppendLineIf(this StringBuilder sb)
        {
            if (sb.Length > 0)
            {
                sb.AppendLine();
            }
            return sb;
        }

    }
}
