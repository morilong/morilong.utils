﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Collections.Concurrent
{
    public static class ConcurrentDictionaryExtension
    {
        public static void AddOrUpdate<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            dic.AddOrUpdate(key, value, (k, v) => { return value; });
        }

        public static bool TryRemove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dic, TKey key)
        {
            return dic.TryRemove(key, out _);
        }

    }
}