﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System.Reflection
{
    public static class MapperHelper
    {
        private static void InternalMapper<Target>(object source, Target target, Type tType, BindingFlags flags, bool isDeep)
        {
            foreach (var sp in source.GetType().GetProperties(flags))
            {
                if (!sp.CanRead)
                    continue;

                var tp = tType.GetProperty(sp.Name, flags);
                if (tp != null && tp.CanWrite)
                {
                    tp.SetValue(target, sp.GetValue(source));
                }
                else if (isDeep && sp.PropertyType.GetTypeCode() == TypeCode.Object)
                {
                    InternalMapper(sp.GetValue(source), target, tType, flags, isDeep);
                }
            }
        }

        private static Target InternalMapper<Target>(object source, BindingFlags flags, bool isDeep)
        {
            var target = Activator.CreateInstance<Target>();
            InternalMapper(source, target, target.GetType(), flags, isDeep);
            return target;
        }

        /// <summary>
        /// 只映射第一层属性
        /// </summary>
        /// <typeparam name="Target"></typeparam>
        /// <param name="source"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public static Target Mapper<Target>(this object source, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public)
        {
            return InternalMapper<Target>(source, flags, false);
        }

        /// <summary>
        /// 深层递归映射
        /// </summary>
        /// <typeparam name="Target"></typeparam>
        /// <param name="source"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public static Target MapperDeep<Target>(this object source, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public)
        {
            return InternalMapper<Target>(source, flags, true);
        }


        /*// https://www.cnblogs.com/lsgsanxiao/p/8205096.html
        public static class TransExpV2<TIn, TOut>
        {
            private static readonly Func<TIn, TOut> _cache = GetFunc();

            private static Func<TIn, TOut> GetFunc(BindingFlags flags)
            {
                var parameterExpression = Expression.Parameter(typeof(TIn), "p");
                var memberBindingList = new List<MemberBinding>();

                foreach (var item in typeof(TOut).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public))
                {
                    if (!item.CanWrite)
                        continue;

                    var property = Expression.Property(parameterExpression, typeof(TIn).GetProperty(item.Name));
                    MemberBinding memberBinding = Expression.Bind(item, property);
                    memberBindingList.Add(memberBinding);
                }

                var memberInitExpression = Expression.MemberInit(Expression.New(typeof(TOut)), memberBindingList.ToArray());
                var lambda = Expression.Lambda<Func<TIn, TOut>>(memberInitExpression, new ParameterExpression[] { parameterExpression });

                return lambda.Compile();
            }

            public static TOut Trans(TIn tIn)
            {
                return _cache(tIn);
            }
        }*/
    }
}
