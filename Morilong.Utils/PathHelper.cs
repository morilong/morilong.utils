﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.IO
{
    public static class PathHelper
    {
        public static string GetSafeFileName(string fileName, char newChar = '-')
        {
            var name = fileName;
            foreach (var item in Path.GetInvalidFileNameChars())
            {
                name = name.Replace(item, newChar);
            }
            return name;
        }

    }
}
