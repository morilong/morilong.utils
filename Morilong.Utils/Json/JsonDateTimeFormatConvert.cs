﻿using System.Text.Json.Serialization;

namespace System.Text.Json
{
    //https://www.cnblogs.com/lenovo_tiger_love/p/12054253.html

    public class JsonDateTimeFormatConvert : JsonConverter<DateTime>
    {
        private readonly string _dateTimeFormat;
        private readonly string _dateFormat;

        public JsonDateTimeFormatConvert(string dateTimeFormat = "yyyy-MM-dd HH:mm:ss", string dateFormat = "yyyy-MM-dd")
        {
            _dateTimeFormat = dateTimeFormat;
            _dateFormat = dateFormat;
        }

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                if (DateTime.TryParse(reader.GetString(), out DateTime dt))
                {
                    return dt;
                }
            }
            return reader.GetDateTime();
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            if (value.Hour == 0 && value.Minute == 0 && value.Second == 0)
            {
                writer.WriteStringValue(value.ToString(_dateFormat));
            }
            else
            {
                writer.WriteStringValue(value.ToString(_dateTimeFormat));
            }
        }

    }
}