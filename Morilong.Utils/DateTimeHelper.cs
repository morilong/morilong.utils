﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace System
{
    public static class DateTimeHelper
    {
        public static int GetMonthDays(int month)
        {
            return Thread.CurrentThread.CurrentUICulture.Calendar.GetDaysInMonth(DateTime.Now.Year, month);
        }


    }
}
