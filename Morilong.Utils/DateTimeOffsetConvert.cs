﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    public static class DateTimeOffsetConvert
    {
        /// <summary>
        /// 将时间转换为格式：yyyy-MM-dd HH:mm:ss 的字符串。
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ToStringEx(this DateTimeOffset dateTime)
        {
            return dateTime.ToString(DateTimeConvert.DateTimeFormat);
        }

    }
}
