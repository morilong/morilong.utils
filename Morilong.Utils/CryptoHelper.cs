﻿using System;
using System.Text;

namespace System.Security
{
    public abstract class CryptoHelper
    {
        /// <summary>
        /// 获取或设置对称算法的密钥。
        /// </summary>
        public virtual byte[] Key { get; set; }

        #region 加密，返回字节数组。 + byte[] EncryptEx(*)
        /// <summary>
        /// 加密，返回字节数组。
        /// </summary>
        /// <param name="data">待加密数据的字节数组</param>
        /// <returns></returns>
        public abstract byte[] EncryptEx(byte[] data);
        /// <summary>
        /// 加密, 返回字节数组
        /// </summary>
        /// <param name="data">待加密字符串</param>
        /// <param name="encoding">数据编码，默认：Encoding.Default</param>
        /// <returns></returns>
        public byte[] EncryptEx(string data, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.Default;

            return this.EncryptEx(encoding.GetBytes(data));
        }
        #endregion

        #region 加密, 返回Base64编码的字符串. + string Encrypt(*)
        /// <summary>
        /// 加密, 返回Base64编码的字符串.
        /// </summary>
        /// <param name="data">待加密数据</param>
        /// <returns></returns>
        public string Encrypt(byte[] data)
        {
            return Convert.ToBase64String(this.EncryptEx(data));
        }
        /// <summary>
        /// 加密, 返回Base64编码的字符串.
        /// </summary>
        /// <param name="data">待加密字符串</param>
        /// <param name="encoding">数据编码，默认：Encoding.Default</param>
        /// <returns></returns>
        public string Encrypt(string data, Encoding encoding = null)
        {
            return Convert.ToBase64String(this.EncryptEx(data, encoding));
        }
        #endregion

        #region 解密由 EncryptEx 方法返回的数据, 返回字节数组 + byte[] DecryptEx(byte[] data)
        /// <summary>
        /// 解密由 EncryptEx 方法返回的数据, 返回字节数组
        /// </summary>
        /// <param name="data">待解密的数据, 由 EncryptEx 方法返回.</param>
        /// <returns></returns>
        public abstract byte[] DecryptEx(byte[] data);

        #endregion

        #region 解密由 Encrypt 方法返回的Base64编码的字符串 + string Decrypt(string s, Encoding encoding = null)
        /// <summary>
        /// 解密由 Encrypt 方法返回的Base64编码的字符串, 返回指定编码的字符串.
        /// </summary>
        /// <param name="s">待解密的Base64编码的字符串, 由 Encrypt 方法返回.</param>
        /// <param name="encoding">返回数据编码，默认：Encoding.Default</param>
        /// <returns></returns>
        public string Decrypt(string s, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.Default;

            return encoding.GetString(this.DecryptEx(Convert.FromBase64String(s)));
        }
        #endregion
    }
}
