﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace System
{
    public static class NumberExtension
    {
        /// <summary>
        /// index == -1
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IsUnMatch(this int index)
        {
            return index == -1;
        }

        #region 保留N位小数但不四舍五入 + decimal Floor(this decimal d, int decimals)
        /// <summary>
        /// 保留N位小数但不四舍五入
        /// </summary>
        /// <param name="d"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        public static double Floor(this double d, int decimals)
        {
            return (double)Floor((decimal)d, decimals);
        }
        /// <summary>
        /// 保留N位小数但不四舍五入
        /// </summary>
        /// <param name="d"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        public static decimal Floor(this decimal d, int decimals)
        {
            if (decimals == 0)
                throw new ArgumentOutOfRangeException();

            decimal n = 1;
            int i = 0;
            do
            {
                n *= 10;
                i++;
            } while (i < decimals);

            return Math.Floor(d * n) / n;
        }

        #endregion

        #region string GetSizeString(this decimal length)

        public static string GetSizeString(this int length, int decimals = 2)
        {
            return GetSizeString((decimal)length, decimals);
        }

        public static string GetSizeString(this long length, int decimals = 2)
        {
            return GetSizeString((decimal)length, decimals);
        }

        public static string GetSizeString(this decimal length, int decimals = 2)
        {
            if (length <= 0)
                return length.ToString();

            if (length < 1024)
                return length + "B";

            for (int i = 1; i <= 4; i++)
            {
                var n = (decimal)Math.Pow(1024, i);
                var max = n * 1024;
                if (length < max)
                {
                    var size = (length / n).Floor(2).ToString("#0.##");
                    switch (i)
                    {
                        case 1:
                            return $"{size}KB";
                        case 2:
                            return $"{size}MB";
                        case 3:
                            return $"{size}GB";
                        case 4:
                            return $"{size}TB";
                    }
                }
            }

            throw new NotImplementedException();
        }

        #endregion

        public static T ToEnum<T>(this int value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }


    }
}
